export enum EPages {
  PHASE_ONE = '/',
  PHASE_TWO = 'phase-two',
}
export enum ESquareOption {
  X = 'X',
  O = 'O',
}

export enum EGameStatus {
  WINNER = 'WINNER.',
  NEXT_PLAYER = 'Next player:',
  DRAW = 'Draw.',
}

export enum EButtonText {
  RESTART = 'RESTART',
  NEXT = 'NEXT',
  PREVIOUS = 'PREVIOUS',
  RESUME = 'RESUME',
  REPLAY = 'REPLAY',
}
