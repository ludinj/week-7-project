export type SquareValue = string | null;

export type IBoardProps = {
  squares: SquareValue[];
  onClick: (i: number) => void;
  active?: string;
};
export interface ISquareProps {
  value: SquareValue;
  onClick?: () => void;
  background?: string;
  position: number;
}

export interface IHistory<T> {
  squares: T[];
  active?: string;
}
