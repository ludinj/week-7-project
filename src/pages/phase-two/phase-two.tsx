import React, { useEffect, useState } from 'react';
import Button from '../../components/button/Button';
import Board from '../../components/phase-two-board/Board';
import Square from '../../components/phase-two-square/Square';
import { useTimeMachine } from '../../hooks/useTimeMachine';
import { calculateWinner } from '../../services/utils';
import { EButtonText, EGameStatus, ESquareOption } from '../../ts/enums';
import { IHistory, SquareValue } from '../../ts/interfaces';
import './phase-two.scss';
const PhaseTwo = () => {
  const initialHistory: IHistory<SquareValue>[] = [
    {
      squares: Array(9).fill(null),
    },
  ];

  const {
    history,
    isTimeTraveling,
    next,
    clickSquare,
    setStepNumber,
    setLastStep,
    previous,
    current,
    resume,
    stepNumber,
    lastStep,
    restart,
  } = useTimeMachine<SquareValue>(initialHistory);

  const [xIsNext, setXIsNext] = useState<boolean>(true);
  const [isStepLeft, setIsStepLeft] = useState<boolean>(true);
  const [isReplaying, setIsReplaying] = useState<boolean>(false);
  const [status, setStatus] = useState<string>('');
  const [windowValue, setWindowValue] = useState<string>('');
  const winner = calculateWinner(history[lastStep].squares);

  useEffect(() => {
    history.forEach((step) => {
      setIsStepLeft(step.squares.some((square) => square === null));
    });
  }, [history]);

  useEffect(() => {
    if (winner) {
      setStatus(EGameStatus.WINNER);
      setWindowValue(winner);
    } else if (isStepLeft) {
      setStatus(EGameStatus.NEXT_PLAYER);
      setWindowValue(xIsNext ? ESquareOption.X : ESquareOption.O);
    } else {
      setStatus(EGameStatus.DRAW);
      setWindowValue('');
    }
  }, [winner, xIsNext, isStepLeft]);

  useEffect(() => {
    // if we are replaying, then start the incrementing timer
    if (isReplaying) {
      let currentStep = 0;
      // create an interval which will increment the step
      const timer = setInterval(() => {
        if (currentStep < lastStep) {
          currentStep = currentStep + 1;
          setStepNumber(currentStep);
        } else {
          // stop here because we have reached the end of steps
          setIsReplaying(false);
        }
      }, 500);

      return () => clearInterval(timer);
    }
  }, [isReplaying, lastStep, setStepNumber]);

  const handleClick = (i: number): void => {
    if (isTimeTraveling || isReplaying) return;
    const newHistory = history.slice();
    const current = newHistory[newHistory.length - 1];
    const squares = current.squares.slice();

    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = xIsNext ? ESquareOption.X : ESquareOption.O;
    clickSquare(
      newHistory.concat([
        {
          squares: squares,
        },
      ])
    );
    setStepNumber(newHistory.length);
    setLastStep(newHistory.length);
    setXIsNext(!xIsNext);
  };
  const handleRestart = () => {
    restart();
    setXIsNext(true);
  };

  return (
    <div className='game'>
      <div className='game-board'>
        <Board squares={current.squares} onClick={(i) => handleClick(i)} />
      </div>
      <div className='game-info'>
        <Button
          text={EButtonText.PREVIOUS}
          onClick={previous}
          disabled={stepNumber <= 1 || isReplaying}
        />
        <Button
          text={EButtonText.NEXT}
          onClick={next}
          disabled={!history[stepNumber + 1] || isReplaying}
        />
        {winner ? (
          <Button
            text={EButtonText.REPLAY}
            onClick={() => setIsReplaying(true)}
            disabled={!winner}
          />
        ) : (
          <Button
            text={EButtonText.RESUME}
            onClick={resume}
            disabled={!isTimeTraveling}
          />
        )}

        <div className='window'>
          {status}
          <Square value={windowValue} position={0} />
        </div>
        <Button
          text={EButtonText.RESTART}
          onClick={handleRestart}
          disabled={stepNumber < 1}
        />
      </div>
    </div>
  );
};

export default PhaseTwo;
