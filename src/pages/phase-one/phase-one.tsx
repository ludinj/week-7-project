import React from 'react';
import Button from '../../components/button/Button';
import Board from '../../components/phase-one-board/Board';
import { useTimeMachine } from '../../hooks/useTimeMachine';
import { EButtonText } from '../../ts/enums';
import { IHistory, SquareValue } from '../../ts/interfaces';
import './phase-one.scss';
const PhaseOne = () => {
  const initialHistory: IHistory<SquareValue>[] = [
    {
      squares: Array(16).fill(null),
    },
  ];
  const {
    history,
    isTimeTraveling,
    next,
    clickSquare,
    setStepNumber,
    setLastStep,
    previous,
    current,
    resume,
    stepNumber,
  } = useTimeMachine<SquareValue>(initialHistory);

  const handleClick = (i: number) => {
    const newHistory = history.slice();
    const current = newHistory[newHistory.length - 1];
    const squares = current.squares.slice();

    if (isTimeTraveling || squares[i]) return;

    squares[i] = `active-${i}`;
    setLastStep(newHistory.length);
    setStepNumber(newHistory.length);
    clickSquare(
      newHistory.concat([
        {
          squares: squares,
          active: `active-${i}`,
        },
      ])
    );
  };
  return (
    <div className='phase-one'>
      <div className='game-board'>
        <Board
          onClick={(i) => handleClick(i)}
          squares={current.squares}
          active={current.active}
        />
      </div>
      <div className='phase__one__game-info'>
        <Button
          text={EButtonText.PREVIOUS}
          onClick={previous}
          disabled={stepNumber <= 1}
        />
        <Button
          text={EButtonText.NEXT}
          onClick={next}
          disabled={!history[stepNumber + 1]}
        />
        <Button
          text={EButtonText.RESUME}
          onClick={resume}
          disabled={!isTimeTraveling}
        />
      </div>
    </div>
  );
};

export default PhaseOne;
