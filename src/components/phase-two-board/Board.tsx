import Square from '../phase-two-square/Square';
import { IBoardProps } from '../../ts/interfaces';
import './board.scss';
const Board: React.FC<IBoardProps> = ({ squares, onClick }) => {
  return (
    <div className='phase__two__board'>
      {squares.map((square, idx) => (
        <Square
          key={idx}
          value={squares[idx]}
          onClick={() => onClick(idx)}
          position={idx}
        />
      ))}
    </div>
  );
};

export default Board;
