import './button.scss';

type ButtonProps = {
  text: string;
  onClick: () => void;
  disabled?: boolean;
};
const Button = ({ text, onClick, disabled }: ButtonProps) => {
  return (
    <button className='button' onClick={onClick} disabled={disabled}>
      {text}
    </button>
  );
};

export default Button;
