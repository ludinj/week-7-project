import { ISquareProps } from '../../ts/interfaces';
import { useEffect, useState } from 'react';
import { colors } from '../../services/constants';
import './square.scss';
const Square: React.FC<ISquareProps> = ({ value, onClick, position }) => {
  const [isActive, setIsActive] = useState<boolean>(false);
  const [activeValue, setActiveValue] = useState<number | null>(null);

  useEffect(() => {
    const getActiveValue = () => {
      if (value) {
        const activeValue = parseInt(value.split('-')[1]);
        setActiveValue(activeValue);
      }
    };
    getActiveValue();
  }, [value, activeValue]);

  useEffect(() => {
    if (activeValue === position) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [position, activeValue]);

  return (
    <div
      className={`phase__one__square ${isActive ? 'active' : ''}`}
      style={{ background: `${colors[position]}` }}
      onClick={onClick}
    ></div>
  );
};
export default Square;
