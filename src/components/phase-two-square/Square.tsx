import { ISquareProps } from '../../ts/interfaces';
import './square.scss';
const Square: React.FC<ISquareProps> = ({ value, onClick }) => {
  return (
    <button className='phase__two__square' onClick={onClick}>
      {value}
    </button>
  );
};
export default Square;
