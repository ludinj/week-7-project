import React from 'react';
import { Link } from 'react-router-dom';
import { EPages } from '../../ts/enums';

import './navbar.scss';
const Navbar = () => {
  return (
    <div className='navbar'>
      <ul>
        <Link to={`${EPages.PHASE_ONE}`}>
          <li>Phase One</li>
        </Link>
        <Link to={`${EPages.PHASE_TWO}`}>
          <li>Phase Two</li>
        </Link>
      </ul>
    </div>
  );
};

export default Navbar;
