import Square from '../phase-one-square/Square';
import { SquareValue } from '../../ts/interfaces';
import { IBoardProps } from '../../ts/interfaces';
import './board.scss';

const Board: React.FC<IBoardProps> = ({ onClick, active, squares }) => {
  return (
    <div className='phase__one__board'>
      {squares.map((square, idx) => (
        <Square
          key={idx}
          value={active ? active : null}
          onClick={() => onClick(idx)}
          position={idx}
        />
      ))}
    </div>
  );
};

export default Board;
