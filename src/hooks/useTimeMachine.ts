import { useState, useEffect } from 'react';

import { IHistory } from '../ts/interfaces';

export const useTimeMachine = <T>(initialHistory: IHistory<T>[]) => {
  const [stepNumber, setStepNumber] = useState<number>(0);
  const [lastStep, setLastStep] = useState<number>(0);
  const [history, setHistory] = useState<IHistory<T>[]>(initialHistory);
  const [isTimeTraveling, setIsTimeTraveling] = useState<boolean>(false);
  const [current, setCurrent] = useState<IHistory<T>>(history[0]);

  useEffect(() => {
    setCurrent(history[stepNumber]);
  }, [history, stepNumber]);

  const next = () => {
    if (history[stepNumber + 1]) {
      setStepNumber((pre) => pre + 1);
      setIsTimeTraveling(true);
      return stepNumber;
    }
  };
  const previous = () => {
    if (stepNumber <= 1) return;
    setStepNumber((pre) => pre - 1);
    setIsTimeTraveling(true);
    return stepNumber;
  };

  const resume = () => {
    setStepNumber(lastStep);
    setIsTimeTraveling(false);
  };

  const clickSquare = (newHistory: IHistory<T>[]) => {
    setHistory(newHistory);
  };

  const restart = () => {
    setStepNumber(0);
    setHistory(initialHistory);
    setIsTimeTraveling(false);
    setLastStep(0);
  };
  return {
    history,
    isTimeTraveling,
    next,
    clickSquare,
    setStepNumber,
    setLastStep,
    previous,
    current,
    resume,
    stepNumber,
    lastStep,
    restart,
  };
};
