import { Routes, Route } from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import PhaseOne from './pages/phase-one/phase-one';
import PhaseTwo from './pages/phase-two/phase-two';
import { EPages } from './ts/enums';
import './app.scss';
function App() {
  return (
    <div className='app'>
      <Navbar />
      <Routes>
        <Route path={`${EPages.PHASE_ONE}`} element={<PhaseOne />} />
        <Route path={`${EPages.PHASE_TWO}`} element={<PhaseTwo />} />
      </Routes>
    </div>
  );
}

export default App;
